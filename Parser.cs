﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMTranslator
{
    static class Parser
    {
        public static Command Parse(string line, string parentFunction, string fileName)
        {
            string trimLine = RemoveComments(line);
            Command command = null;

            // splitting on 'new char[0]' will cause the method to split on whitespace
            string[] parts = trimLine.Split(new char[0], StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length == 1)
            {
                if (parts[0] == Encoder.Return) 
                    command = ParseFunctionCmd(parts[0]);
                else
                    command = ParseArithmeticLogicalCmd(parts[0]);
            }
            else if (parts.Length == 2)
                command = ParseFlowCmd(parts[0], parts[1].TrimEnd(), parentFunction);
            else if (parts.Length == 3)
            {
                if (parts[0] == Encoder.Push || parts[0] == Encoder.Pop)
                    command = ParseStackCmd(parts[0], parts[1], parts[2].TrimEnd(), fileName);
                else
                    command = ParseFunctionCmd(parts[0], parts[1], parts[2].TrimEnd());
            }
            else
                throw new InvalidOperationException("Cannot parse invalid line: " + line);

            return command;
        }


        private static ArithmeticLogicalCmd ParseArithmeticLogicalCmd(string cmd)
        {
            var command = new ArithmeticLogicalCmd(cmd, "//" + cmd);
            return command;
        }


        private static FlowCmd ParseFlowCmd(string cmd, string label, string parentFunction)
        {
            var comment = string.Format("//{0} {1}", cmd, label);
            var command = new FlowCmd(cmd, label, comment, parentFunction);
            return command;
        }


        private static FunctionCmd ParseFunctionCmd(string cmd, string functionName = null, string numArgs = null)
        {
            var comment = "//" + cmd;
            if (functionName != null)
                comment += " " + functionName + " " + numArgs;

            var command = new FunctionCmd(cmd, comment, functionName, numArgs);
            return command;
        }


        private static StackCmd ParseStackCmd(string cmd, string arg1, string arg2, string fileName)
        {
            string comment= string.Format("//{0} {1} {2}", cmd, arg1, arg2);
            var command = new StackCmd(cmd, arg1, arg2, comment, fileName);
            return command;
        }


        private static string RemoveComments(string line)
        {
            int index = line.IndexOf("//");
            if (index > -1)
                return line.Substring(0, index).TrimEnd();

            return line.TrimEnd();
        }
    }
}
