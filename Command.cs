﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMTranslator
{
    abstract class Command
    {
        readonly string _cmd;
        readonly string _comment;

        protected Command(string cmd, string comment)
        {
            _cmd = cmd;
            _comment = comment;
        }

        public string Cmd
        {
            get { return _cmd; }
        }

        public string Comment
        {
            get { return _comment; }
        }
    }
}
