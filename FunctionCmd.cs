﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMTranslator
{
    class FunctionCmd : Command
    {
        readonly string _functionName;
        readonly string _number;

        public FunctionCmd(string cmd, string comment, string functionName = null, string number = null)
            : base(cmd, comment)
        {
            _functionName = functionName;
            _number = number;
        }

        public string FunctionName
        {
            get { return _functionName; }
        }

        public string Number
        {
            get { return _number; }
        }
    }
}
