﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMTranslator
{
    class FlowCmd : Command
    {
        readonly string _label;
        readonly string _parentFunction;

        public FlowCmd(string cmd, string label, string comment, string parentFunction)
            : base(cmd, comment)
        {
            _label = label;
            _parentFunction = parentFunction;
        }

        public string Label
        {
            get { return _label; }
        }

        public string ParentFunction
        {
            get { return _parentFunction; }
        }
    }
}
