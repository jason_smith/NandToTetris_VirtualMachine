﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMTranslator
{
    class Program
    {
        const string VmFileExt = ".vm";
        const string AsmFileExt = ".asm";

        private static IList<string> GetFiles(string[] args)
        {
            IList<string> files = null;

            if (args.Length == 1 && Directory.Exists(args[0]))
                files = Directory.GetFiles(args[0], "*" + VmFileExt);
            else
                files = args;

            return files;
        }


        private static string GetOutFilePath(string[] args)
        {
            string outFile = null;

            if (args.Length == 1 && Directory.Exists(args[0]))
                outFile = Path.Combine(args[0], Path.GetFileName(args[0]) + AsmFileExt);
            else
                outFile = Path.GetFileNameWithoutExtension(args[0]) + AsmFileExt;

            return outFile;
        }


        static void Main(string[] args)
        {
            IList<string> files = GetFiles(args);
            ValidateFiles(files);

            var allCommands = new List<Command>();
            foreach (string file in files)
            {
                Console.WriteLine("Processing file: " + file);

                IList<Command> vmCommands;
                bool success = ParseFile(file, out vmCommands);
                if (success)
                    allCommands.AddRange(vmCommands);
                else
                    return;

                Console.WriteLine();
            }
            
            string outFile = GetOutFilePath(args);
            Translate(allCommands, outFile);
        }


        private static bool ParseFile(string file, out IList<Command> cmds)
        {
            int lineNum = 0;
            int cmdCount = 0;
            cmds = new List<Command>();

            try
            {
                string currentFunction = null;
                string fileName = Path.GetFileNameWithoutExtension(file);

                IEnumerable<string> lines = File.ReadLines(file);
                foreach (string rawLine in lines)
                {
                    lineNum++;

                    string line = rawLine.TrimStart();
                    if (line.StartsWith("//") || string.IsNullOrEmpty(line))
                        continue;

                    Command cmd = Parser.Parse(line, currentFunction, fileName);
                    cmds.Add(cmd);
                    cmdCount++;

                    var funcCmd = cmd as FunctionCmd;
                    if (funcCmd != null && funcCmd.Cmd == Encoder.Function)
                        currentFunction = funcCmd.FunctionName;
                }

                Console.WriteLine("VM commands parsed: " + cmdCount.ToString());

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Parse failed at line " + lineNum.ToString() + ": " + ex.Message);
                return false;
            }
        }


        private static void Translate(IList<Command> commands, string outFile)
        {
            StreamWriter writer = null;
            try
            {
                writer = File.CreateText(outFile);

                string preamble = Encoder.GetPreamble();
                writer.WriteLine(preamble);

                foreach (Command cmd in commands)
                {
                    string asm = Encoder.Encode(cmd);
                    writer.WriteLine(asm);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed to translate command. " + ex.Message);
            }
            finally
            {
                if (writer != null)
                    writer.Close();
            }
        }


        private static void ValidateFiles(IList<string> files)
        {
            if (files == null || files.Count == 0)
            {
                Console.WriteLine("No files specified.");
                throw new ArgumentNullException();
            }

            foreach (string file in files)
            {
                string ext = Path.GetExtension(file);
                if (!string.Equals(ext, VmFileExt, StringComparison.OrdinalIgnoreCase))
                {
                    Console.WriteLine("Invalid file type: " + ext + ". Only files of type '" + VmFileExt + "' are allowed.");
                    throw new ArgumentException();
                }

                if (!File.Exists(file))
                {
                    Console.WriteLine("File not found: " + file);
                    throw new ArgumentException();
                }
            }
        }
    }
}
