﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMTranslator
{
    class StackCmd : Command
    {
        readonly string _segment;
        readonly string _value;
        readonly string _fileName;

        public StackCmd(string cmd, string segment, string value, string comment, string fileName)
            : base(cmd, comment)
        {
            _segment = segment;
            _value = value;
            _fileName = fileName;
        }

        public string FileName
        {
            get { return _fileName; }
        }

        public string Segment
        {
            get { return _segment; }
        }

        public string Value
        {
            get { return _value; }
        }
    }
}
