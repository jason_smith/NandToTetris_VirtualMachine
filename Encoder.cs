﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMTranslator
{
    static class Encoder
    {
        internal const string Push = "push";
        internal const string Pop = "pop";
        internal const string Return = "return";
        internal const string Function = "function";
        const string SP = "SP";
        const string LCL = "LCL";
        const string ARG = "ARG";
        const string THIS = "THIS";
        const string THAT = "THAT";
        const string Local = "local";
        const string Argument = "argument";
        const string This = "this";
        const string That = "that";
        const string Pointer = "pointer";
        const string Temp = "temp";
        const string Constant = "constant";
        const string Static = "static";
        const string Add = "add";
        const string Subtract = "sub";
        const string Negate = "neg";
        const string Equal = "eq";
        const string GreaterThan = "gt";
        const string LessThan = "lt";
        const string And = "and";
        const string Or = "or";
        const string Not = "not";
        const string Goto = "goto";
        const string IfGoto = "if-goto";
        const string Label = "label";
        const string Call = "call";
        const string FrameVar = "frame";
        const string RetVar = "ret";

        const string If = "IF_";
        const string Else = "ELSE_";
        const string EndIf = "END_IF_";

        static int _ifBlockCount;
        static int _retCount;

        static readonly string IncrementSPAsm;
        static readonly string DoublePopStackAsm;
        static readonly string SinglePopStackAsm;


        static Encoder()
        {
            IncrementSPAsm = "@SP\r\nM=M+1";
            SinglePopStackAsm = "@SP\r\nM=M-1\r\nA=M\r\nD=M\r\n";
            DoublePopStackAsm = SinglePopStackAsm + "@SP\r\nM=M-1\r\nA=M\r\n";
        }


        private static string BuildBinaryLogicalAsm(ArithmeticLogicalCmd cmd)
        {
            var sb = new StringBuilder();
            sb.Append(DoublePopStackAsm);

            if (cmd.Cmd == And)
                sb.AppendLine("M=M&D");
            else if (cmd.Cmd == Or)
                sb.AppendLine("M=M|D");
            else
                throw new InvalidOperationException("Unrecognized binary logical operator: " + cmd.Cmd);

            sb.AppendLine(IncrementSPAsm);

            return sb.ToString();
        }


        private static string BuildEqualityAsm(ArithmeticLogicalCmd cmd)
        {
            var sb = new StringBuilder();
            sb.Append(DoublePopStackAsm);
            sb.AppendLine("D=M-D");
            sb.AppendLine(GetLabelRef(If, _ifBlockCount));

            if (cmd.Cmd == Equal)
                sb.AppendLine("D;JEQ");
            else if (cmd.Cmd == LessThan)
                sb.AppendLine("D;JLT");
            else if (cmd.Cmd == GreaterThan)
                sb.AppendLine("D;JGT");
            else
                throw new InvalidOperationException("Unrecognized equality command: " + cmd.Cmd);

            sb.AppendLine(GetLabelRef(Else, _ifBlockCount));
            sb.AppendLine("0;JMP");
            sb.AppendLine(GetLabel(If, _ifBlockCount));
            sb.AppendLine("@SP");
            sb.AppendLine("A=M");
            sb.AppendLine("M=-1");
            sb.AppendLine(GetLabelRef(EndIf, _ifBlockCount));
            sb.AppendLine("0;JMP");
            sb.AppendLine(GetLabel(Else, _ifBlockCount));
            sb.AppendLine("@SP");
            sb.AppendLine("A=M");
            sb.AppendLine("M=0");
            sb.AppendLine(GetLabel(EndIf, _ifBlockCount));
            sb.AppendLine(IncrementSPAsm);

            _ifBlockCount++;

            return sb.ToString();
        }


        private static string BuildFunctionCallAsm(FunctionCmd cmd)
        {
            string pushFormat = "D={0}\r\n@SP\r\nA=M\r\nM=D";

            var sb = new StringBuilder();
            sb.AppendLine(cmd.Comment);
            sb.AppendLine(GetLabelRef(Return, _retCount));
            sb.AppendLine(string.Format(pushFormat, "A"));
            sb.AppendLine(IncrementSPAsm);
            sb.AppendLine("@" + LCL);
            sb.AppendLine(string.Format(pushFormat, "M"));
            sb.AppendLine(IncrementSPAsm);
            sb.AppendLine("@" + ARG);
            sb.AppendLine(string.Format(pushFormat, "M"));
            sb.AppendLine(IncrementSPAsm);
            sb.AppendLine("@" + THIS);
            sb.AppendLine(string.Format(pushFormat, "M"));
            sb.AppendLine(IncrementSPAsm);
            sb.AppendLine("@" + THAT);
            sb.AppendLine(string.Format(pushFormat, "M"));
            sb.AppendLine(IncrementSPAsm);
            sb.AppendLine("@5");
            sb.AppendLine("D=A");
            sb.AppendLine("@" + cmd.Number);
            sb.AppendLine("D=D+A");
            sb.AppendLine("@SP");
            sb.AppendLine("D=M-D");
            sb.AppendLine("@" + ARG);
            sb.AppendLine("M=D");
            sb.AppendLine("@SP");
            sb.AppendLine("D=M");
            sb.AppendLine("@" + LCL);
            sb.AppendLine("M=D");
            sb.AppendLine("@" + cmd.FunctionName);
            sb.AppendLine("0;JMP");
            sb.AppendLine(GetLabel(Return, _retCount));

            _retCount++;

            return sb.ToString();
        }


        private static string BuildFunctionDeclarationAsm(FunctionCmd cmd)
        {
            string initLocals = cmd.FunctionName + "!initLocals";
            string body = cmd.FunctionName + "!body";

            var sb = new StringBuilder();
            sb.AppendLine(cmd.Comment);
            sb.AppendLine("(" + cmd.FunctionName + ")");
            sb.AppendLine("@SP");
            sb.AppendLine("D=M");
            sb.AppendLine("@LCL");
            sb.AppendLine("M=D");
            sb.AppendLine("@" + cmd.Number);
            sb.AppendLine("D=A");
            sb.AppendLine("(" + initLocals + ")");
            sb.AppendLine("@" + body);
            sb.AppendLine("D;JEQ");
            sb.AppendLine("@SP");
            sb.AppendLine("A=M");
            sb.AppendLine("M=0");
            sb.AppendLine("@SP");
            sb.AppendLine("M=M+1");
            sb.AppendLine("D=D-1");
            sb.AppendLine("@" + initLocals);
            sb.AppendLine("0;JMP");
            sb.AppendLine("(" + body + ")");

            return sb.ToString();
        }


        private static string BuildFunctionReturnAsm(FunctionCmd cmd)
        {
            string decFrameAndLoadDReg = "@" + FrameVar + "\r\nM=M-1\r\nA=M\r\nD=M";

            var sb = new StringBuilder();
            sb.AppendLine(cmd.Comment);
            sb.AppendLine("@" + LCL);
            sb.AppendLine("D=M");
            sb.AppendLine("@" + FrameVar);
            sb.AppendLine("M=D");
            sb.AppendLine("@5");
            sb.AppendLine("A=D-A");
            sb.AppendLine("D=M");
            sb.AppendLine("@" + RetVar);
            sb.AppendLine("M=D");
            sb.AppendLine("@SP");
            sb.AppendLine("A=M-1");
            sb.AppendLine("D=M");
            sb.AppendLine("@" + ARG);
            sb.AppendLine("A=M");
            sb.AppendLine("M=D");
            sb.AppendLine("@" + ARG);

            sb.AppendLine("D=M+1");
            //sb.AppendLine("D=M");

            sb.AppendLine("@SP");
            sb.AppendLine("M=D");
            sb.AppendLine(decFrameAndLoadDReg);
            sb.AppendLine("@" + THAT);
            sb.AppendLine("M=D");
            sb.AppendLine(decFrameAndLoadDReg);
            sb.AppendLine("@" + THIS);
            sb.AppendLine("M=D");
            sb.AppendLine(decFrameAndLoadDReg);
            sb.AppendLine("@" + ARG);
            sb.AppendLine("M=D");
            sb.AppendLine(decFrameAndLoadDReg);
            sb.AppendLine("@" + LCL);
            sb.AppendLine("M=D");
            sb.AppendLine("@" + RetVar);
            sb.AppendLine("A=M");
            sb.AppendLine("0;JMP");

            return sb.ToString();
        }


        private static string BuildPopToSegmentAsm(StackCmd cmd)
        {
            var sb = new StringBuilder();
            sb.AppendLine(cmd.Comment);

            if (cmd.Segment == Static)
            {
                sb.AppendLine("@SP");
                sb.AppendLine("M=M-1");
                sb.AppendLine("A=M");
                sb.AppendLine("D=M");
                sb.Append("@");
                sb.AppendLine(GetAsmMemSegment(cmd));
                sb.AppendLine("M=D");
            }
            else if (cmd.Segment != Temp && cmd.Segment != Pointer)
            {
                sb.Append("@");
                sb.AppendLine(GetAsmMemSegment(cmd));
                sb.AppendLine("D=M");
                sb.Append("@");
                sb.AppendLine(cmd.Value);
                sb.AppendLine("D=D+A");
                sb.AppendLine("@R15");
                sb.AppendLine("M=D");
                sb.AppendLine("@SP");
                sb.AppendLine("M=M-1");
                sb.AppendLine("A=M");
                sb.AppendLine("D=M");
                sb.AppendLine("@R15");
                sb.AppendLine("A=M");
                sb.AppendLine("M=D");
            }
            else
            {
                sb.AppendLine("@SP");
                sb.AppendLine("M=M-1");
                sb.AppendLine("A=M");
                sb.AppendLine("D=M");
                sb.Append("@");
                sb.AppendLine(GetAsmMemSegment(cmd));
                sb.AppendLine("M=D");
            }

            return sb.ToString();
        }


        private static string BuildPushConstAsm(StackCmd cmd)
        {
            var sb = new StringBuilder();
            sb.AppendLine(cmd.Comment);
            sb.Append("@");
            sb.AppendLine(cmd.Value);
            sb.AppendLine("D=A");
            sb.AppendLine("@SP");
            sb.AppendLine("A=M");
            sb.AppendLine("M=D");
            sb.AppendLine(IncrementSPAsm);

            return sb.ToString();
        }


        private static string BuildPushFromSegmentAsm(StackCmd cmd)
        {
            var sb = new StringBuilder();
            sb.AppendLine(cmd.Comment);
            sb.Append("@");
            sb.AppendLine(GetAsmMemSegment(cmd));
            sb.AppendLine("D=M");

            if (cmd.Segment == Local || cmd.Segment == Argument || cmd.Segment == This || cmd.Segment == That)
            {
                sb.AppendLine("@" + cmd.Value);
                sb.AppendLine("A=D+A");
                sb.AppendLine("D=M");
            }

            sb.AppendLine("@SP");
            sb.AppendLine("A=M");
            sb.AppendLine("M=D");
            sb.AppendLine(IncrementSPAsm);

            return sb.ToString();
        }


        public static string Encode(Command vmCmd)
        {
            string encoding = null;

            var arithCmd = vmCmd as ArithmeticLogicalCmd;
            if (arithCmd != null)
                encoding = EncodeArithmeticLogicalCmd(arithCmd);
            else
            {
                var stackCmd = vmCmd as StackCmd;
                if (stackCmd != null)
                    encoding = EncodeStackCmd(stackCmd);
                else
                {
                    var flowCmd = vmCmd as FlowCmd;
                    if (flowCmd != null)
                        encoding = EncodeFlowCmd(flowCmd);
                    else
                    {
                        var functionCmd = vmCmd as FunctionCmd;
                        if (functionCmd != null)
                            encoding = EncodeFunctionCmd(functionCmd);
                    }
                }
            }

            if (encoding == null)
                throw new InvalidOperationException("Failed to encode command: " + vmCmd.Comment);

            if (!encoding.EndsWith("\r\n"))
                encoding += "\r\n";

            return encoding;
        }


        private static string EncodeArithmeticLogicalCmd(ArithmeticLogicalCmd cmd)
        {
            string asm = cmd.Comment + Environment.NewLine;

            switch (cmd.Cmd)
            {
                case Add:
                    asm += DoublePopStackAsm + "D=D+M\r\n@SP\r\nA=M\r\nM=D\r\n" + IncrementSPAsm;
                    break;
                case Subtract:
                    asm += DoublePopStackAsm + "D=M-D\r\n@SP\r\nA=M\r\nM=D\r\n" + IncrementSPAsm;
                    break;
                case Negate:
                    asm += "@SP\r\nM=M-1\r\nA=M\r\nM=-M\r\n" + IncrementSPAsm;
                    break;
                case Equal:
                case GreaterThan:
                case LessThan:
                    asm += BuildEqualityAsm(cmd);
                    break;
                case And:
                case Or:
                    asm += BuildBinaryLogicalAsm(cmd);
                    break;
                case Not:
                    asm += "@SP\r\nM=M-1\r\nA=M\r\nM=!M\r\n" + IncrementSPAsm;
                    break;
            }

            return asm;
        }


        private static string EncodeFlowCmd(FlowCmd cmd)
        {
            string encoding = null;
            string labelName = cmd.ParentFunction + "$" + cmd.Label;

            if (cmd.Cmd == Label)
                encoding = "(" + labelName + ")\r\n";
            else if (cmd.Cmd == Goto)
                encoding = "@" + labelName + "\r\n0;JMP\r\n";
            else if (cmd.Cmd == IfGoto)
                encoding = SinglePopStackAsm + "@" + labelName + "\r\nD;JNE\r\n";
            else
                throw new InvalidOperationException("Unrecognized flow command: " + cmd.Cmd);

            return encoding;
        }


        private static string EncodeFunctionCmd(FunctionCmd cmd)
        {
            string encoding = null;

            if (cmd.Cmd == Function)
                encoding = BuildFunctionDeclarationAsm(cmd);
            else if (cmd.Cmd == Call)
                encoding = BuildFunctionCallAsm(cmd);
            else if (cmd.Cmd == Return)
                encoding = BuildFunctionReturnAsm(cmd);
            else
                throw new InvalidOperationException("Unrecognized function command: " + cmd.Cmd);

            return encoding;
        }
       

        private static string EncodeStackCmd(StackCmd cmd)
        {
            string encoding = null;

            if (cmd.Cmd == Push)
                encoding = (cmd.Segment == Constant) ? BuildPushConstAsm(cmd) : BuildPushFromSegmentAsm(cmd);
            else if (cmd.Cmd == Pop)
                encoding = BuildPopToSegmentAsm(cmd);
            else
                throw new InvalidOperationException("Unrecognized stack command: " + cmd.Cmd);

            return encoding;
        }


        private static string GetAsmMemSegment(StackCmd cmd)
        {
            switch (cmd.Segment)
            {
                case Local:
                    return LCL;
                case Argument:
                    return ARG;
                case This:
                    return THIS;
                case That:
                    return THAT;
                case Temp:
                    return "R" + (int.Parse(cmd.Value) + 5);
                case Pointer:
                    return "R" + (int.Parse(cmd.Value) + 3);
                case Static:
                    return cmd.FileName + "." + cmd.Value;
            }

            throw new InvalidOperationException("Unrecognized memory segment: " + cmd.Segment);
        }


        private static string GetLabel(string label, int count)
        {
            return "(" + label + count.ToString() + ")";
        }


        private static string GetLabelRef(string label, int count)
        {
            return "@" + label + count.ToString();
        }


        public static string GetPreamble()
        {
            string initSP = "//Preamble\r\n@256\r\nD=A\r\n@SP\r\nM=D\r\n";
            //string preambleRet = "@preamble_returnAddr\r\nD=A\r\n@SP\r\nA=M\r\nM=D\r\n" + IncrementSPAsm;
            ////return initSP + preambleRet + "\r\n@Sys.init\r\n0;JMP\r\n";
            //return initSP + "@Sys.init\r\n0;JMP\r\n"; ;
            Command cmd = Parser.Parse("call Sys.init 0", string.Empty, "Sys");
            string encoding = Encode(cmd);
            return initSP + encoding;
        }
    }
}
